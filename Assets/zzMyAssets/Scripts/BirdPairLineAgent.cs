﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class BirdPairLineAgent : MonoBehaviour
{

    public void BH_SetOriginParent (Transform parent)
    {
        m_originAnchor.transform.parent = parent;
        m_originAnchor.transform.localPosition = Vector2.zero;
    }

    public void BH_SetTargetParent(Transform parent)
    {
        m_targetAnchor.transform.parent = parent;
        m_targetAnchor.transform.localPosition = Vector2.zero;


    }




    public void BH_SetVisibility (bool targetVisibility)
    {
        m_lineRenderer.enabled = targetVisibility;
        //////askedForMove = true;
        if (targetVisibility)
            StartCoroutine(PositionFixer());
    }

    public IEnumerator PositionFixer ()
    {
        m_lineRenderer.enabled = false;
        yield return null;
        m_targetAnchor.transform.localPosition = new Vector2(1, 1);
        Debug.Log("--- jump 1  at " + Time.time);
        yield return null;
        m_targetAnchor.transform.localPosition = new Vector2(-1, -1);
        Debug.Log("--- jump 2  at " + Time.time);
        yield return null;
        m_targetAnchor.transform.localPosition = Vector2.zero;
        Debug.Log("--- jump 3  at " + Time.time);
        m_lineRenderer.enabled = true;

    }

    //////public void Update()
    //////{
        
    //////    if (askedForMove)
    //////    {
    //////        Vector2 step = Vector2.MoveTowards(m_targetAnchor.transform.localPosition, new Vector2(0, 0), .1f);
    //////        m_targetAnchor.transform.position = m_targetAnchor.transform.position + step;
    //////        if (m_targetAnchor.transform. == new Vector2(1,1))

    //////    }
    //////}

    //////public bool askedForMove = false;
    [SerializeField]
    GameObject m_originAnchor;
    [SerializeField]
    public GameObject m_targetAnchor;
    [SerializeField]
    UILineRenderer m_lineRenderer;
}
