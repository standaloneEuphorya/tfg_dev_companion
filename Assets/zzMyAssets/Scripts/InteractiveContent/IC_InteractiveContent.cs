﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class IC_InteractiveContent : MonoBehaviour
{
    public virtual void LSTR_OwnerGoesToVisible(CA_TrackedContentAgent owner)
    {
        m_visualContent.SetActive(true);

    }

    void LSTR_OwnerExitsFromVisible (CA_TrackedContentAgent owner)
    {
        m_visualContent.SetActive(false);
    }

    public virtual void Awake()
    {

        m_ownerTCA.OnEntersToVisible += LSTR_OwnerGoesToVisible;
        m_ownerTCA.OnExitsVisible += LSTR_OwnerExitsFromVisible;

    }
    public virtual void Start()
    {
        m_visualContent.SetActive(false);
    }


    [SerializeField]
    GameObject m_visualContent;
    [SerializeField]
    protected CA_TrackedContentAgent m_ownerTCA;
}
