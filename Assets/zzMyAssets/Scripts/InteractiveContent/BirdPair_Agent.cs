﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BirdPair_Agent : MonoBehaviour
{

    public void LSTR_SoundButtonClicked ()
    {
        OnSoundButtonClicked?.Invoke(this);
    }

    public void LSTR_BirdButtonClicked ()
    {
        OnBirdButtonClicked?.Invoke(this);
    }

    public void BH_MarkAsSoundSelected ()
    {
        
        m_references.m_imgSoundSelected.enabled = true;
    }

    public void BH_SetLineTarget (Transform target)
    {
        m_lineAgent.BH_SetVisibility(true);
        m_lineAgent.BH_SetTargetParent(target);

    }

    public void BH_MarkAsSoundUnselected ()
    {

        if (m_currentTarget != null)
            m_currentTarget.m_isPointedBySomeone = false;

        m_currentTarget = null;
        m_references.m_imgSoundSelected.enabled = false;
        m_lineAgent.BH_SetVisibility(false);
    }

    public bool BH_IsProperlyPointing ()
    {
        if (m_currentTarget == this)
            return true;
        else
            return false;
    }
    
    public bool BH_IsPointingAnything()
    {
        if (m_currentTarget == null)
            return false;
        else
            return true;

    }

    public void Init (DisplayableData data)
    {
        m_thisData = data;
        m_references.m_imageButton.GetComponent<Image>().sprite  = data.m_image;
        m_references.m_soundButton.GetComponent<Image>().sprite = data.m_image;
        m_references.m_imgSoundSelected.enabled = false;
        m_currentTarget = null;
        m_isPointedBySomeone = false;

        m_lineAgent.BH_SetOriginParent(m_references.m_soundButton.transform);
        m_lineAgent.BH_SetTargetParent(this.transform);
        m_lineAgent.BH_SetVisibility(false);
    }

    public void BH_SetImagePosition (GameObject targetAnchor)
    {
        m_references.m_imageButton.transform.parent = targetAnchor.transform;
        m_references.m_imageButton.transform.localPosition = Vector2.zero;
    }
    public void BH_SetSoundPosition(GameObject targetAnchor)
    {
        m_references.m_soundButton.transform.parent = targetAnchor.transform;
        m_references.m_soundButton.transform.localPosition = Vector2.zero;
    }



    [SerializeField]
    public BirdPair_AgentReferences m_references;

    public DisplayableData m_thisData;

    public BirdPair_Agent m_currentTarget;
    public bool m_isPointedBySomeone = false;

    public BirdPairLineAgent m_lineAgent;

    public delegate void BirdPairAgentAction(BirdPair_Agent caller);
    public event BirdPairAgentAction OnSoundButtonClicked;
    public event BirdPairAgentAction OnBirdButtonClicked;
    
    [System.Serializable]
    public class BirdPair_AgentReferences
    {
        public Button m_imageButton;
        
        public Button m_soundButton;

        public Image m_imgSoundSelected;
    }
}
