﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindRight_Agent : MonoBehaviour
{
    
    public void LSTR_ButtonTouched ()
    {
        if (OnClick != null)
            OnClick(this);
    }

    public int  BH_PerformButtonClicked (bool canSelect)
    {
        if (m_isSelected)
        {
            BH_Unselect();
            return -1;
        }
        else if (canSelect)
        {
            BH_Select();
            return 1;
        }

        return 0;
    }
    void BH_Select ()
    {
        m_isSelected = true;
        m_isSelectedVisualFeedback.enabled = true;
    }

    void BH_Unselect ()
    {
        m_isSelected = false;
        m_isSelectedVisualFeedback.enabled = false;
    }


    public void BH_WarmUp (bool isRight, DisplayableDataWithGallery data)
    {
        m_isSelected = false;
        m_isRight = isRight;
        m_thisImage.sprite = data.m_imageArray[0];
        m_isSelectedVisualFeedback.enabled = false;
    }

    private void Awake()
    {
        BH_Unselect();
    }
    public bool m_isRight;
    public bool m_isSelected;

    public Image m_isSelectedVisualFeedback;
    public Image m_thisImage;
    

    public event FindRightAgentAction OnClick;
    public delegate void FindRightAgentAction(FindRight_Agent caller);
}


