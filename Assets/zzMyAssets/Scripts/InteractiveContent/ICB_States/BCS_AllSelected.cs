﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCS_AllSelected : BCS_InteractiveContentBirdState
{
    public override void Enter()
    {
        base.Enter();

        m_owner.m_references.m_btnResolve.gameObject.SetActive(false);
        m_owner.m_references.m_btnRepeat.gameObject.SetActive(true);
    }

    public override void Exit()
    {
        base.Exit();
    }
}
