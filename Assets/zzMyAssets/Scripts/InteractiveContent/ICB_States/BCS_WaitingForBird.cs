﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCS_WaitingForBird : BCS_InteractiveContentBirdState
{
    public override void Enter()
    {
        base.Enter();

        foreach (BirdPair_Agent item in m_owner.m_references.m_birdAgents)
        {
            item.OnSoundButtonClicked += m_owner.LSTR_SoundButtonWasClicked;
            item.OnBirdButtonClicked += m_owner.LSTR_BirdButtonWasPressed;
        }

   //     m_owner.OnResolutionButtonClick += m_owner.LSTR_ResolveButtonClick;


    }

    public override void Exit()
    {
        base.Exit();

        foreach (BirdPair_Agent item in m_owner.m_references.m_birdAgents)
        {
            item.OnSoundButtonClicked -= m_owner.LSTR_SoundButtonWasClicked;
            item.OnBirdButtonClicked -= m_owner.LSTR_BirdButtonWasPressed;
        }

     //   m_owner.OnResolutionButtonClick -= m_owner.LSTR_ResolveButtonClick;
    }
}
