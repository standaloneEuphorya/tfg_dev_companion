﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IC_FindRights : IC_InteractiveContent
{

    #region events
    public void LSTR_StartNewGame (CA_TrackedContentAgent owner)
    {
        BH_ReshuffleAgents();

        m_currentMatchRights = Random.Range(1, 5);

        m_references.m_feedbackTextRequired.text = "Encuentra " + m_currentMatchRights.ToString();
        m_currentlySelectedButtons = 0;

        for (int i = 0; i < 10; i++)
        {
            if (i < m_currentMatchRights)
                m_references.m_allAgents[i].BH_WarmUp(true, m_references.m_rihgtData[i]);
            else
                m_references.m_allAgents[i].BH_WarmUp(false, m_references.m_wrongData[i-m_currentMatchRights]);
        }

        m_references.m_btnPlayAgain.gameObject.SetActive(false);
        m_references.m_btnResolve.gameObject.SetActive(true);
        m_references.m_feedbackTextResult.gameObject.SetActive(false);
        m_references.m_vitoryIcon.gameObject.SetActive(false);

        foreach (FindRight_Agent item in m_references.m_allAgents)
        {
            item.GetComponent<Button>().interactable = true;
        }
    }

    public void LSTR_ResolutionButtonWasClicked (FindRight_Agent caller)
    {
        if (m_currentlySelectedButtons == m_currentMatchRights)
            m_currentlySelectedButtons += caller.BH_PerformButtonClicked(false);
        else
            m_currentlySelectedButtons += caller.BH_PerformButtonClicked(true);
    }

    public void LSTR_ResolveButtonClicked ()
    {
        if (m_currentlySelectedButtons != m_currentMatchRights)
            return;

        int correct = 0;
        foreach (FindRight_Agent item in m_references.m_allAgents)
        {
            if (item.m_isRight && item.m_isSelected)
                correct++;
        }



        m_references.m_feedbackTextResult.text = correct.ToString() + " aciertos";
        m_references.m_feedbackTextResult.gameObject.SetActive(true);
        if (correct == m_currentMatchRights)
        {
            m_references.m_btnResolve.gameObject.SetActive(false);
            m_references.m_btnPlayAgain.gameObject.SetActive(true);
            m_references.m_vitoryIcon.gameObject.SetActive(true);
            AudioSource.PlayClipAtPoint(m_references.m_victorySound, Camera.main.transform.position);
            foreach (FindRight_Agent item in m_references.m_allAgents)
            {
                item.GetComponent<Button>().interactable = false;
            }

        } else
        {
            AudioSource.PlayClipAtPoint(m_references.m_wrongSound, Camera.main.transform.position);
            m_references.m_btnResolve.gameObject.SetActive(true);
            m_references.m_btnPlayAgain.gameObject.SetActive(false);
        }
    }

    public void LSTR_PlayAgainButton ()
    {
        LSTR_StartNewGame(null);
    }
    #endregion


    public void BH_ReshuffleAgents ()
    {
        for (int i = 0; i < m_references.m_allAgents.Length; i ++)
        {
            FindRight_Agent strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_allAgents.Length - 1);
            strayAgent = m_references.m_allAgents[strayAgentIndex];
            m_references.m_allAgents[strayAgentIndex] = m_references.m_allAgents[i];
            m_references.m_allAgents[i] = strayAgent;
        }

        for (int i = 0; i < m_references.m_rihgtData.Length; i++)
        {
            DisplayableDataWithGallery strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_rihgtData.Length - 1);
            strayAgent = m_references.m_rihgtData[strayAgentIndex];
            m_references.m_rihgtData[strayAgentIndex] = m_references.m_rihgtData[i];
            m_references.m_rihgtData[i] = strayAgent;
        }

        for (int i = 0; i < m_references.m_wrongData.Length; i++)
        {
            DisplayableDataWithGallery strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_wrongData.Length - 1);
            strayAgent = m_references.m_wrongData[strayAgentIndex];
            m_references.m_wrongData[strayAgentIndex] = m_references.m_wrongData[i];
            m_references.m_wrongData[i] = strayAgent;
        }
    }

    //////public void Update()
    //////{
    //////    if (Input.anyKey)
    //////    {
    //////        BH_ReshuffleAgents();
    //////    }
    //////}


    public override void Awake()
    {
        base.Awake();

        m_ownerTCA.OnEntersToVisible += LSTR_StartNewGame;

        foreach ( FindRight_Agent item in m_references.m_allAgents)
        {
            item.OnClick += LSTR_ResolutionButtonWasClicked;
        }
    }

    [SerializeField]
    int m_currentMatchRights;
    [SerializeField]
    int m_currentlySelectedButtons;
    [SerializeField]
    IC_FindRightsReferences m_references;

    [System.Serializable]
    public class IC_FindRightsReferences
    {
        public FindRight_Agent[] m_allAgents;

        public DisplayableDataWithGallery [] m_rihgtData;
        public DisplayableDataWithGallery[] m_wrongData;

        public Button m_btnResolve;
        public Button m_btnPlayAgain;
        public Text m_feedbackTextResult;
        public Text m_feedbackTextRequired;
        public Image m_vitoryIcon;

        public AudioClip m_victorySound;
        public AudioClip m_wrongSound;
    }
}
