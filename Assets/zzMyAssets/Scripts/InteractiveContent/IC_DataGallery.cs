﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IC_DataGallery : IC_InteractiveContent
{
    public void LSTR_ClickOnNext ()
    {
        m_currentlyOnDisplay++;

        UpdateButtonsStatus();
        UpdateScrollStatus();
    }

    public void LSTR_ClickOnPrev()
    {
        m_currentlyOnDisplay--;
        UpdateButtonsStatus();
        UpdateScrollStatus();
    }
    public void UpdateScrollStatus ()
    {
        m_references.m_scrollableObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-m_currentlyOnDisplay * m_horizontalScrollDelta, 0);

    }


    public void UpdateButtonsStatus ()
    {
        if (m_currentlyOnDisplay == 0)
        {
            m_references.m_btnPrev.interactable = false;
            if (m_displayableData.Length == 1)
                m_references.m_btnNex.interactable = false;
            else
                m_references.m_btnNex.interactable = true;
        }
        else if (m_currentlyOnDisplay == m_displayableData.Length-1)
        {
            m_references.m_btnNex.interactable = false;
            if (m_displayableData.Length == 1)
                m_references.m_btnPrev.interactable = false;
            else
                m_references.m_btnPrev.interactable = true;
        } 
        else
        {
            m_references.m_btnPrev.interactable = true;
            m_references.m_btnNex.interactable = true;
        }
        

    }

    public override void Awake()
    {
        base.Awake();
        m_currentlyOnDisplay = 0;

        UpdateButtonsStatus();

        m_horizontalScrollDelta = m_references.m_scrollableObject.GetComponentInParent<Canvas>().GetComponent<RectTransform>().sizeDelta.x;//.GetComponent<RectTransform>().sizeDelta.x;
        for (int i = 0; i < m_displayableData.Length; i++)
        {
   
            GalleryDataContainer newContaniner = (Instantiate(m_stencils.m_dataContainer, m_references.m_scrollableObject.transform) as GalleryDataContainer).Init(m_displayableData[i]);
            //newContaniner.gameObject.transform.parent = m_references.m_scrollableObject.transform;
            RectTransform rTrans = newContaniner.GetComponent<RectTransform>();
            rTrans.anchoredPosition = new Vector2(i *m_horizontalScrollDelta , 0);
        }

    }

    int m_currentlyOnDisplay; 
    float m_horizontalScrollDelta; 
    public DisplayableDataWithGallery[] m_displayableData;

    public IC_DataGalleryStencils m_stencils;
    public IC_DataGalleryReferences m_references;
    [System.Serializable]
    public class IC_DataGalleryStencils
    {
        public GalleryDataContainer m_dataContainer;
        
    }

    [System.Serializable]
    public class IC_DataGalleryReferences
    {
        public Transform m_scrollableObject;
        public RectTransform m_rctTransformReference;

        public Button m_btnPrev;
        public Button m_btnNex;
    }
}
