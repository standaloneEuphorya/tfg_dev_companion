﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IC_BirdsChallenge : IC_InteractiveContent, IStatable
{
    #region listeners to c# events
    void LSTR_StartNewGame(CA_TrackedContentAgent owner)
    {
        BH_ReshuffleData();
        BH_ReshuffleBottomAnchors();
        BH_ReshuffleTopAnchors();
        //foreach (BirdPair_Agent item in m_references.m_birdAgents)
        for (int i = 0; i < m_references.m_birdAgents.Length; i++)
        {
            m_references.m_birdAgents[i].Init(m_references.m_data[i]);
            m_references.m_birdAgents[i].BH_SetImagePosition(m_references.m_topAnchors[i]);
            m_references.m_birdAgents[i].BH_SetSoundPosition(m_references.m_bottomAnchors[i]);
        }
        //// BH_ReshuffleDataList();

        m_references.m_gameEndImage.enabled = false;
        m_references.m_txtAciertos.enabled = false;
        m_references.m_btnResolve.gameObject.SetActive(true);
        m_references.m_btnRepeat.gameObject.SetActive(false);
        SM_IdleToWaitingForSound();
    }
    
    void LSTR_EndMode (CA_TrackedContentAgent owner)
    {
        SM_AnyToIdle();
    }
    public void LSTR_TryResolution()
    {
        if (!BH_AllPairsSet())
        {
            AudioSource.PlayClipAtPoint(m_references.m_soundWrong, Camera.main.transform.position);
            return;
        }

        //////foreach (BirdPair_Agent item in m_references.m_birdAgents)
        //////{
        //////    if (item.m_currentTarget == null)
        //////    {
        //////        AudioSource.PlayClipAtPoint(m_references.m_soundWrong, Camera.main.transform.position);
        //////        return;
        //////    }
        //////}



       // SM_WaitingForBirdToAllSelected();
        int hits = 0;

        foreach (BirdPair_Agent item in m_references.m_birdAgents)
        {
            if (item.BH_IsProperlyPointing())
                hits++;
        }

        m_references.m_txtAciertos.text = hits.ToString() + " aciertos";
        m_references.m_txtAciertos.enabled = true;

        if (hits == m_references.m_birdAgents.Length)
        {
            AudioSource.PlayClipAtPoint(m_references.m_soundRight, Camera.main.transform.position);
            m_references.m_gameEndImage.enabled = true;
            // m_references.m_btnResolve.enabled = true;
            // m_references.m_btnRepeat.enabled = false;
                
            SM_WaitingForSoundToAllSelected();
        } else
        {
            AudioSource.PlayClipAtPoint(m_references.m_soundWrong, Camera.main.transform.position);
        }
    }

    public void LSTR_SoundButtonWasClicked (BirdPair_Agent caller)
    {
        if (caller.BH_IsPointingAnything())
        {
            caller.BH_MarkAsSoundUnselected();
            return;
        }

        if (caller != m_soundSelectedFrom)
        //if (!caller.BH_IsProperlyPointing())
        {
            m_soundSelectedFrom?.BH_MarkAsSoundUnselected();
            m_soundSelectedFrom = caller;
            m_soundSelectedFrom.BH_MarkAsSoundSelected();
            m_references.m_mainAudioSource.clip = caller.m_thisData.m_sound;
            m_references.m_mainAudioSource.Play();
            SM_WaitingForSoundToWaitingForBird();
        } else
        {
            m_soundSelectedFrom?.BH_MarkAsSoundUnselected();
            m_soundSelectedFrom = null;
            m_references.m_mainAudioSource.Stop();
            SM_WaitingForBirdToWaitingForSound();
        }
    }

    public void LSTR_SoundButtonWasPressedWhileWaitingForBird (BirdPair_Agent caller)
    {

    }

    public void LSTR_BirdButtonWasPressed (BirdPair_Agent caller)
    {
        m_references.m_mainAudioSource.Stop();
        
        if (caller.m_isPointedBySomeone)
        {
            AudioSource.PlayClipAtPoint(m_references.m_soundWrong, Camera.main.transform.position);
            return;
        }

        m_soundSelectedFrom.BH_SetLineTarget(caller.m_references.m_imageButton.transform);
        m_soundSelectedFrom.m_currentTarget = caller;
        caller.m_isPointedBySomeone = true;
        SM_WaitingForBirdToWaitingForSound();

    }
    #endregion

    #region listeners to unity events
    ////public void LSTR_ResolveButtonClick ()
    ////{
    ////    OnResolutionButtonClick?.Invoke();
    ////}

    public void LSTR_ReplayButtonClick()
    {
        LSTR_StartNewGame(null);
    }

   
    #endregion

    #region state management
    public void SM_IdleToWaitingForSound()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(BCT_IdleToWaitingForSound)) as BCS_InteractiveContentBirdState;
    }

    public void SM_WaitingForSoundToWaitingForBird()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(BCT_WaitingForSoundToWaitingForbird)) as BCS_InteractiveContentBirdState;
    }

    public void SM_WaitingForBirdToWaitingForSound()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(BCT_WaitingForBirdToWaitingForSound)) as BCS_InteractiveContentBirdState;
    }

    public void SM_WaitingForSoundToAllSelected()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(BCT_WaitingForSoundToAllSelected)) as BCS_InteractiveContentBirdState;
    }
    public void SM_AnyToIdle ()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(BCT_FromAnyToIdle)) as BCS_InteractiveContentBirdState;
    }
    #endregion

    void BH_ReshuffleTopAnchors()
    {
        for (int i = 0; i < m_references.m_topAnchors.Length; i++)
        {
            GameObject strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_topAnchors.Length - 1);
            strayAgent = m_references.m_topAnchors[strayAgentIndex];
            m_references.m_topAnchors[strayAgentIndex] = m_references.m_topAnchors[i];
            m_references.m_topAnchors[i] = strayAgent;
        }
    }

    void BH_ReshuffleBottomAnchors()
    {
        for (int i = 0; i < m_references.m_bottomAnchors.Length; i++)
        {
            GameObject strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_bottomAnchors.Length - 1);
            strayAgent = m_references.m_bottomAnchors[strayAgentIndex];
            m_references.m_bottomAnchors[strayAgentIndex] = m_references.m_bottomAnchors[i];
            m_references.m_bottomAnchors[i] = strayAgent;
        }
    }

    void BH_ReshuffleData()
    {
        for (int i = 0; i < m_references.m_data.Length; i++)
        {
            DisplayableData strayAgent;
            int strayAgentIndex = Random.Range(0, m_references.m_data.Length - 1);
            strayAgent = m_references.m_data[strayAgentIndex];
            m_references.m_data[strayAgentIndex] = m_references.m_data[i];
            m_references.m_data[i] = strayAgent;
        }
    }

    bool BH_AllPairsSet()
    {
        foreach (BirdPair_Agent item in m_references.m_birdAgents)
        {
            if (item.m_currentTarget == null)
                return false;
        }

        return true;
    }
    //////void BH_ReshuffleDataList ()
    //////{
    //////    for (int i = 0; i < m_references.m_birdAgents.Length; i++)
    //////    {
    //////        BirdPair_Agent strayAgent;
    //////        int strayAgentIndex = Random.Range(0, m_references.m_birdAgents.Length - 1);
    //////        strayAgent = m_references.m_birdAgents[strayAgentIndex];
    //////        m_references.m_birdAgents[strayAgentIndex] = m_references.m_birdAgents[i];
    //////        m_references.m_birdAgents[i] = strayAgent;
    //////    }
    //////}


        

    public override void Awake()
    {
        base.Awake();

        for (int i = 0; i < m_references.m_birdAgents.Length; i ++)
        {
            m_references.m_birdAgents[i].m_lineAgent = m_references.m_birdPairLineAgents[i];
        }

        m_ownerTCA.OnEntersToVisible += LSTR_StartNewGame;
        m_ownerTCA.OnExitsVisible += LSTR_EndMode;


        m_states.m_pool = new GenericTransitionPool<IC_BirdsChallenge>();

        // m_states.m_current = m_states.m_pool.BH_AddState(new BCS_WaitingForSound().Init(this) as BCS_WaitingForSound) as BCS_InteractiveContentBirdState;
        m_states.m_current = m_states.m_pool.BH_AddState(new BCS_Idle().Init(this) as BCS_Idle) as BCS_InteractiveContentBirdState;
        m_states.m_pool.BH_AddState(new BCS_WaitingForSound().Init(this) as BCS_WaitingForSound);
        m_states.m_pool.BH_AddState(new BCS_WaitingForBird().Init(this) as BCS_WaitingForBird);
        m_states.m_pool.BH_AddState(new BCS_AllSelected().Init(this) as BCS_AllSelected);


        m_states.m_pool.BH_AddTransition(new BCT_IdleToWaitingForSound().Init(this), new[] { typeof(BCS_Idle), typeof (BCS_AllSelected)}, typeof(BCS_WaitingForSound));
        m_states.m_pool.BH_AddTransition(new BCT_WaitingForBirdToAllSelected().Init(this), new[] { typeof(BCS_WaitingForBird) }, typeof(BCS_AllSelected));
        m_states.m_pool.BH_AddTransition(new BCT_WaitingForBirdToWaitingForSound().Init(this), new[] { typeof(BCS_WaitingForBird) }, typeof(BCS_WaitingForSound));
        m_states.m_pool.BH_AddTransition(new BCT_WaitingForSoundToWaitingForbird().Init(this), new[] { typeof(BCS_WaitingForSound) }, typeof(BCS_WaitingForBird));
        m_states.m_pool.BH_AddTransition(new BCT_FromAnyToIdle().Init(this), null, typeof(BCS_Idle));
        m_states.m_pool.BH_AddTransition(new BCT_WaitingForSoundToAllSelected().Init(this), new[] { typeof(BCS_WaitingForSound) }, typeof(BCS_AllSelected));

        Debug.Log("XYZ");
    }

    public IC_BirdsChallengeReferences m_references;
    
    public BirdPair_Agent m_soundSelectedFrom;

    [SerializeField]
    [Header("Watches")]
    IC_BirdsChallengeState m_states;

   


    public event ResolutionAction OnResolutionButtonClick;
    public delegate void ResolutionAction ();

    [System.Serializable]
    public class IC_BirdsChallengeState
    {
        public BCS_InteractiveContentBirdState m_current;
        public GenericTransitionPool<IC_BirdsChallenge> m_pool;
    }

    [System.Serializable]
    public class IC_BirdsChallengeReferences {
        public GameObject []m_topAnchors;
        public GameObject[] m_bottomAnchors;
        public DisplayableData[] m_data;

        public BirdPair_Agent[] m_birdAgents;
        public BirdPairLineAgent[] m_birdPairLineAgents;
        public AudioSource m_mainAudioSource;
        public Image m_gameEndImage;
        public Text m_txtAciertos;

        public Button m_btnResolve;
        public Button m_btnRepeat;

        public AudioClip m_soundWrong;
        public AudioClip m_soundRight;
    }
}
