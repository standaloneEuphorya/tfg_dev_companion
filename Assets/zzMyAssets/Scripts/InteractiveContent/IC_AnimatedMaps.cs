﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class IC_AnimatedMaps : IC_InteractiveContent, IStatable
{

    public void LSTR_AbortRouteButtonClicked ()
    {
        LSTR_RuteDisplayFinished();
    }

    public void LSTR_RouteButtonClicked(int index)
    {
        m_currentPlayableDirector = m_references.m_playableDirectors[index];
        SM_IdleToDisplayingRoute();
    }
    
    public void LSTR_RuteDisplayFinished ()
    {
        SM_DisplayingAnimationToIdle();
        Debug.Log("---");
    }

    void SM_IdleToDisplayingRoute ()
    {
       m_states.m_current =  m_states.m_pool.BH_PerformTransition(typeof(ICMT_IdleToDisplayingRoute)) as ICMS_InteractiveContentMapState;
    }

    public void SM_DisplayingAnimationToIdle ()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(ICMT_DisplayingRouteToIdle)) as ICMS_InteractiveContentMapState;
    }

    public void Update()
    {
        m_states.m_pool.BH_RefreshCurrent();
    }
    public override void Awake()
    {
        base.Awake();
        m_mainMenuController = FindObjectOfType<MainMenuController>();
        m_states.m_pool = new GenericTransitionPool<IC_AnimatedMaps>();

        m_states.m_current = m_states.m_pool.BH_AddState(new ICMS_Idle().Init(this) as ICMS_Idle) as ICMS_InteractiveContentMapState;
        m_states.m_pool.BH_AddState(new ICMS_DisplayingRoute().Init(this) as ICMS_DisplayingRoute);

        m_states.m_pool.BH_AddTransition(new ICMT_IdleToDisplayingRoute().Init(this), new[] { typeof(ICMS_Idle) }, typeof(ICMS_DisplayingRoute));
        m_states.m_pool.BH_AddTransition(new ICMT_DisplayingRouteToIdle().Init(this), new[] { typeof(ICMS_DisplayingRoute) }, typeof(ICMS_Idle));
        
    }

    public override void Start()
    {
        base.Start();
        foreach (PlayableDirector item in m_references.m_playableDirectors)
        {
            item.gameObject.SetActive(false);
        }
    }


    [SerializeField]
    IC_AnimatedMapsStates m_states;

    
    public MainMenuController m_mainMenuController;
    [HideInInspector]
    public PlayableDirector m_currentPlayableDirector;
    [SerializeField]
    public IC_AnimatedMapsReferences m_references;

    [System.Serializable]
    public class IC_AnimatedMapsReferences
    {
        public PlayableDirector[] m_playableDirectors;

        public Button[] m_allButtons;

        public GameObject m_routeButtons;
        public GameObject m_inRouteButtons;
    }

    [System.Serializable]
    public class IC_AnimatedMapsStates
    {
        public ICMS_InteractiveContentMapState m_current;
        public GenericTransitionPool <IC_AnimatedMaps> m_pool;
    }


    ////public event RouteButtonClickAction OnRouteButtonClicked;
    ////public delegate void RouteButtonClickAction(int buttonIndex);
}
