﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ICMS_Idle : ICMS_InteractiveContentMapState
{
    public override void Enter()
    {
        base.Enter();

        m_owner.m_references.m_routeButtons.SetActive(true);
        m_owner.m_references.m_inRouteButtons.SetActive(false);
        foreach (Button item in m_owner.m_references.m_allButtons)
        {
            item.gameObject.SetActive(true);
        }
    }


    public override void Exit()
    {
        base.Enter();

        foreach (Button item in m_owner.m_references.m_allButtons)
        {
            item.gameObject.SetActive(false);
        }
    }


}
