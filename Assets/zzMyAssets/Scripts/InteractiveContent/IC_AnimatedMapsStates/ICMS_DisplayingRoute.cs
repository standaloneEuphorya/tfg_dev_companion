﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ICMS_DisplayingRoute : ICMS_InteractiveContentMapState
{
    public override void Enter()
    {
        base.Enter();
        m_owner.m_references.m_routeButtons.SetActive(false);
        m_owner.m_references.m_inRouteButtons.SetActive(true);

        m_owner.m_currentPlayableDirector.gameObject.SetActive(true);
        m_owner.m_currentPlayableDirector.time = 0;
        m_owner.m_currentPlayableDirector.Play();
        m_timelineEndsAt = (float) m_owner.m_currentPlayableDirector.duration + Time.time  ;

        m_owner.m_mainMenuController.OnExitButtonClicked += m_owner.LSTR_RuteDisplayFinished;
    }

    public override void Refresh()
    {
        base.Refresh();
        if (Time.time > m_timelineEndsAt + 1)
            m_owner.SM_DisplayingAnimationToIdle();
    }

    public override void Exit()
    {
        base.Exit();

        m_owner.m_currentPlayableDirector.time = 0;
        m_owner.m_currentPlayableDirector.Stop();
        m_owner.m_currentPlayableDirector.Evaluate();
        

        m_owner.m_mainMenuController.OnExitButtonClicked -= m_owner.LSTR_RuteDisplayFinished;
        m_owner.m_currentPlayableDirector.gameObject.SetActive(false);
    }

    float m_timelineEndsAt; 
}
