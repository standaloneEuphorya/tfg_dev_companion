﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class IC_SimplePlayableDirector : IC_InteractiveContent
{
    public override void LSTR_OwnerGoesToVisible(CA_TrackedContentAgent owner)
    {
        base.LSTR_OwnerGoesToVisible(owner);
        m_playableDirector.time = 0;
        if (m_trailRenderer != null)
            m_trailRenderer.Clear();
        m_playableDirector.Play();
    }

    public void LSTR_TrailClear ()
    {
        StartCoroutine(CR_DelayedTrailClear());
    }

    IEnumerator CR_DelayedTrailClear()
    {
        yield return null;
        if (m_trailRenderer != null)
            m_trailRenderer.Clear();
    }

    public override void Awake()
    {
        base.Awake();
        m_playableDirector = GetComponent<PlayableDirector>();
    }

    private void OnEnable()
    {
        if (m_trackerSimulator == null)
            return;

        m_trackerSimulator.OnTrackergLost += LSTR_TrailClear;
        m_trackerSimulator.OnTrackerFound += LSTR_TrailClear;

        m_ownerTCA.OnExitsVisible += LSTR_Close;
    }

    private void OnDisable()
    {
        if (m_trackerSimulator == null)
            return;

        m_trackerSimulator.OnTrackergLost -= LSTR_TrailClear;
        m_trackerSimulator.OnTrackerFound -= LSTR_TrailClear;

        m_ownerTCA.OnExitsVisible -= LSTR_Close;

    }

    public void LSTR_Close (CA_TrackedContentAgent caller)
    {
        m_playableDirector.Stop();
    }
    

    PlayableDirector m_playableDirector;
    public TrailRenderer m_trailRenderer;

    public TrackerSimulator m_trackerSimulator;
}
