﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DisplayableData", menuName = "ScriptableObjects/DisplayableData", order = 1)]
[System.Serializable]
public class DisplayableData : ScriptableObject
{


    public string m_name;
    public string m_description;
    public Sprite m_image;
    public AudioClip m_sound;
}
