﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DisplayableData", menuName = "ScriptableObjects/DisplayableDataWithGallery", order = 1)]
[System.Serializable]
public class DisplayableDataWithGallery : DisplayableData
{

    public Sprite[] m_imageArray;
}
