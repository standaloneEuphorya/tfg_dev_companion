﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public void BH_ShowMenu ()
    {
        m_references.m_mainCanvas.enabled = true;
    }

    public void BH_HideMenu ()
    {
        m_references.m_mainCanvas.enabled = false;
        OnExitButtonClicked?.Invoke();

    }


    private void Start()
    {
        BH_HideMenu();
    }
    [SerializeField]
    MainMenuControllerReferences m_references;

    
    [System.Serializable]
    public class MainMenuControllerReferences
    {
        public Canvas m_mainCanvas;
        public Button m_helpButton;
        public Button m_closeButton;
    }


    public event Action OnExitButtonClicked;
}


