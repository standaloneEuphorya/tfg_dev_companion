﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericState <T> : ScriptableObject //where T : IStatable 
{
    public virtual void Enter()
    {
        Debug.Log("-- enters " + this.GetType());

    }

    public virtual void Exit ()
    {
        Debug.Log("-- exits " + this.GetType());
    }

    public virtual void Refresh ()
    {

    }


    public GenericState<T> Init ( T owner)
    {
        m_owner = owner;
        return this;
    } 

   protected T m_owner;
}
