﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITrackingObject

{
     event TrackingEvent OnTrackerFound;
     event TrackingEvent OnTrackergLost;
}


public delegate void TrackingEvent ();