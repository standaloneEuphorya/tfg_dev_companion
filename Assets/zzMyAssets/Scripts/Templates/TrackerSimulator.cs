﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerSimulator : MonoBehaviour, ITrackingObject
{
 

    public void Update()
    {
        if (m_simulateFound)
        {
            m_simulateFound = false;
            m_simulateLost = false;

            if (OnTrackerFound != null)
                OnTrackerFound();

            return;
        } 

        if (m_simulateLost)
        {
            m_simulateFound = false;
            m_simulateLost = false;

            if (OnTrackergLost != null)
                OnTrackergLost();

            return;

        }
    }


    void BSTR_TrackerFound()
    {
        transform.localPosition = Vector3.zero;
        Debug.Log("Tracker found --" + name);
        OnTrackerFound?.Invoke();
    }

    void BSTR_TrackerLost()
    {
        transform.localPosition = new Vector3(1000, 1000, 1000);
        Debug.Log("Tracker found --" + name);
        OnTrackergLost?.Invoke();
    }

    void OnEnable()
    {
        if (m_customTrackable != null)
        {
            m_customTrackable.OnMarkerFound += BSTR_TrackerFound;
            m_customTrackable.OnMarkerLost += BSTR_TrackerLost;
        }
    }

    void OnDisable()
    {
        if (m_customTrackable != null)
        {
            m_customTrackable.OnMarkerFound -= BSTR_TrackerFound;
            m_customTrackable.OnMarkerLost -= BSTR_TrackerLost;
        }
    }

    void Awake ()
    {
        m_customTrackable = GetComponentInParent<CustomTrackableEventHandler>();
    }

    public bool m_followTracker = true;

    public bool m_simulateFound;
    public bool m_simulateLost;

    public event TrackingEvent OnTrackerFound;
    public event TrackingEvent OnTrackergLost;

    public CustomTrackableEventHandler m_customTrackable;
}
