﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class GenericTransition <T> where T: IStatable
{
/*
    public void BH_PerformTransition ()
    {
        m_origin.Exit();
        BH_TransitionActions();
        m_target.Enter();


    }
  */
  
    ////public void BH_PerformCurrentExit()
    ////{
    ////    m_origin.Exit();
    ////}

    public void BH_PerforTargetEnter ()
    {
        m_target.Enter();
    }


    public virtual GenericState<T> BH_TransitionActions ()
    {

        return m_target;
    }

    public GenericTransition<T> Init ( T owner)
    {
        m_owner = owner;
        return this;
    }

    public bool BH_IsOrigin(GenericState<T> origin)
    {
        if (m_origin.Contains(origin))
            return true;
        else
            return false;

        //////if (m_origin.GetType() == origin.GetType())
        //////    return true;
        //////else
        //////    return false;
    }

    public void WarmUp (GenericState<T> [] origins, GenericState<T> target)
    {
        m_origin = new List<GenericState<T>>();

        //m_origin = origin;
        foreach (GenericState<T> item in origins)
        {
            if (item != target)
                m_origin.Add(item);
        }


        m_target = target;

    }




    protected List<GenericState<T>> m_origin;
    protected GenericState<T> m_target;
    protected T m_owner;

    private IEnumerator m_performTransition;





}
