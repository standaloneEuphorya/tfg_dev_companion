﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenericTransitionPool<T>  where T : IStatable
{


    public GenericState<T> BH_AddState (GenericState<T> newState)
    {
        m_states.Add(newState);

        if (m_currentState == null)
        {
            m_currentState = newState;
            m_currentState.Enter();
            return m_currentState;
        }

        return null;
    }

    public void BH_AddTransition (GenericTransition<T> newTransition, System.Type [] origins, System.Type target)
    {

        //GenericState<T> [] inListOrigin = m_states.Where(x => x.GetType() == origin);        
        List<GenericState<T>> inListOrigin = new List<GenericState<T>>();
        if (origins != null)
        {
            foreach (System.Type item in origins)
            {
                GenericState<T> aOrigin = m_states.Single(x => x.GetType() == item);
                inListOrigin.Add(aOrigin);
            }
        } else
        {
            foreach (GenericState<T> item in m_states)
            {
                if (item.GetType() != target)
                    inListOrigin.Add(item);
            }
        }
        GenericState<T> inListTarget = m_states.Single(x => x.GetType() == target);

        newTransition.WarmUp(inListOrigin.ToArray(), inListTarget);

        

        m_transitions.Add(newTransition);
    }

    public void BH_RefreshCurrent()
    {
        m_currentState.Refresh();
    }


    public GenericState<T> BH_PerformTransition (System.Type transition)
    {
        ////if (transition.GetType() != m_currentSTate.GetType())
        ////    return null;
        
        
           
        GenericTransition<T> requestedTransition = m_transitions.Single(x => x.GetType() == transition);

        if (!requestedTransition.BH_IsOrigin(m_currentState))
        {
            Debug.Log("<color=red>Error performing transition</color> ");
            return m_currentState;
        }

        //TODO:: consider modifying this to make the calls through a 'refresh current transition' method to allow non-instant transitions
        //       the idea would be to have something similar to m_currentState for transitions and a Transition.Refresh similar to State.Refresh
        //       called from BH_RefreshCurrent where checks (as 'transition time reached') can be performed
        //requestedTransition.BH_PerformCurrentExit();
        m_currentState.Exit();
        m_currentState = requestedTransition.BH_TransitionActions();
        requestedTransition.BH_PerforTargetEnter();

        return m_currentState;

    }
    private List <GenericState<T>> m_states = new List<GenericState<T>>();
    private List<GenericTransition<T>> m_transitions = new List<GenericTransition<T>>();

    public GenericState<T> m_currentState;


}
