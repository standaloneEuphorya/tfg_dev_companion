﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatableObject : MonoBehaviour, IStatable
{


    public void Start()
    {
        m_pool = new GenericTransitionPool<StatableObject>();



       m_currentSTate = m_pool.BH_AddState(ScriptableObject.CreateInstance<SOS_Inactive>().Init(this) as SOS_Inactive) as SOS_StatableObjectState;
        
        m_pool.BH_AddState(ScriptableObject.CreateInstance<SOS_Active>().Init(this) as SOS_Active);



        //m_pool.BH_AddTransition(typeof(SOT_ActiveToInactive, typeof(SOS_Active), typeof(SOS_Inactive));
        

        m_pool.BH_AddTransition(new SOT_ActiveToInactive().Init(this), new[] { typeof(SOS_Active) }, typeof(SOS_Inactive));
        m_pool.BH_AddTransition(new SOT_InactiveToActive().Init(this),new[] { typeof(SOS_Inactive) }, typeof(SOS_Active));


       m_currentSTate =  m_pool.BH_PerformTransition(typeof(SOT_InactiveToActive)) as SOS_StatableObjectState;
        m_currentSTate = m_pool.BH_PerformTransition(typeof(SOT_ActiveToInactive)) as SOS_StatableObjectState;

        m_currentSTate = m_pool.BH_PerformTransition(typeof(SOT_ActiveToInactive)) as SOS_StatableObjectState;

    }


    public void Update()
    {
        m_pool.BH_RefreshCurrent();
    }


    public  SOS_StatableObjectState m_currentSTate;
    public GenericTransitionPool<StatableObject> m_pool;
}
