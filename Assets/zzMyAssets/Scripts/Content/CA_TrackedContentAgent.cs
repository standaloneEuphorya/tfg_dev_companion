﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_TrackedContentAgent : MonoBehaviour, IStatable
{

    #region State Management
    public void SM_FromInactiveToActive()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(CAT_InactiveToActive)) as CAS_ContentAgentState;
    }

    public void SM_FromActiveToVisible ()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(CAT_ActiveToVisible)) as CAS_ContentAgentState;
    }

    public void SM_FromAnyToInactive ()
    {
        m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(CAT_AnyToInactive)) as CAS_ContentAgentState;
    }

    #endregion

    #region event callers
    public void EvtCaller_OnEnterVisible()
    {
        if (OnEntersToVisible != null)
            OnEntersToVisible(this);
    }

    public void EvtCaller_OnExitVisible ()
    {
        if (OnExitsVisible != null)
            OnExitsVisible(this);
    }

    #endregion

    public void Update()
    {
        m_states.m_pool.BH_RefreshCurrent();
    }

    private void Awake()
    {
        m_relatedTrackers = new List<ITrackingObject>();

        foreach (GameObject item in m_relatedTrackersGOs)
        {
            m_relatedTrackers.Add(item.GetComponentWithInterface<ITrackingObject>());
        }

        m_states.m_pool = new GenericTransitionPool<CA_TrackedContentAgent>();

        m_states.m_current = m_states.m_pool.BH_AddState(new CAS_Inactive().Init(this) as CAS_Inactive) as CAS_ContentAgentState;
        m_states.m_pool.BH_AddState(new CAS_Active().Init(this) as CAS_Active);
        m_states.m_pool.BH_AddState(new CAS_Visible().Init(this) as CAS_Visible);

        m_states.m_pool.BH_AddTransition(new CAT_InactiveToActive().Init(this), new[] { typeof(CAS_Inactive) }, typeof(CAS_Active));
        m_states.m_pool.BH_AddTransition(new CAT_ActiveToVisible().Init(this), new[] { typeof(CAS_Active) }, typeof(CAS_Visible));
        m_states.m_pool.BH_AddTransition(new CAT_AnyToInactive().Init(this), null, typeof(CAS_Inactive));


        Debug.Log("XYZ");

        ////m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(CAT_InactiveToActive)) as CAS_ContentAgentState;
        ////m_states.m_current = m_states.m_pool.BH_PerformTransition(typeof(CAT_InactiveToActive)) as CAS_ContentAgentState;


    }

    [SerializeField][Header ("Watches")]
    CA_TrackedContentAgentState m_states;

    [Header ("Settings")]
    public bool m_followTrackerWhileVisible = true;


    [Header ("References")]    
    public GameObject[] m_relatedTrackersGOs;
    public List<ITrackingObject> m_relatedTrackers;
    public GameObject m_contentHolder;


    public event TrackedContentAgentEvent OnEntersToVisible;
    public event TrackedContentAgentEvent OnExitsVisible;
    //public event TrackedContentAgentEvent OnEnters
}

[System.Serializable]
public class CA_TrackedContentAgentState
{
    public CAS_ContentAgentState m_current;
    public GenericTransitionPool<CA_TrackedContentAgent> m_pool;
}

 public delegate void TrackedContentAgentEvent (CA_TrackedContentAgent caller);

