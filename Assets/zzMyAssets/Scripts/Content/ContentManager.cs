﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentManager : MonoBehaviour
{


    #region listeners UNITY EVENTS
    public void LSTR_CloseButtonClicked()
    {
        m_references.m_mainMenuController.BH_HideMenu();
        m_currentTCA.SM_FromAnyToInactive();
        m_currentTCA = null;

        foreach (CA_TrackedContentAgent item in m_allTCAs)
        {

            item.SM_FromInactiveToActive();
         
        }
    }

    public void LSTR_HelpButtonClicked()
    {

    }
    #endregion

    #region listeners C# EVENTS
    public void LSTR_TrackerContentAgentEntersVisible(CA_TrackedContentAgent caller)
    {
        m_currentTCA = caller;

        foreach (CA_TrackedContentAgent item in m_allTCAs)
        {
            if (item != m_currentTCA)
            {
                item.SM_FromAnyToInactive();
            }
        }

        m_references.m_mainMenuController.BH_ShowMenu();
    }

    #endregion
    private void Awake()
    {
        m_allTCAs = GetComponentsInChildren<CA_TrackedContentAgent>();
    }

    private void Start()
    {
        foreach (CA_TrackedContentAgent item in m_allTCAs)
        {
            item.SM_FromInactiveToActive();
            item.OnEntersToVisible += LSTR_TrackerContentAgentEntersVisible;
        }
    }

    public CA_TrackedContentAgent[] m_allTCAs;

    public CA_TrackedContentAgent m_currentTCA;

    // --//ALL BUT ONE to INACTIVE-->SUBSCRIBES TO CA_TrackedContentAgent.OnVisible event. All but the event trigger goes to inactive
    //ALL GOES to ACTIVE--> subscribes to mainMenu 'exit' button. All CA_TrackedContentAgents go to 'Active' (this is 'not visible but ready to load when marker is found)

    [SerializeField]
    ContentManagerReferences m_references;
}

[System.Serializable]
public class ContentManagerReferences
{
    public MainMenuController m_mainMenuController;
}
