﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAS_Active : CAS_ContentAgentState
{
    public override void Enter()
    {
        base.Enter();
        foreach (ITrackingObject item in m_owner.m_relatedTrackers)
        {
            item.OnTrackerFound += m_owner.SM_FromActiveToVisible;
        }

    }

    public override void Exit()
    {
        base.Exit();
        foreach (ITrackingObject item in m_owner.m_relatedTrackers)
        {
            item.OnTrackerFound -= m_owner.SM_FromActiveToVisible;
        }
    }
}
