﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAS_Visible : CAS_ContentAgentState
{
    public override void Enter()
    {
        base.Enter();

        m_owner.EvtCaller_OnEnterVisible();     

        //TODO::
        //suscribir aqui 'esconder visible' (move to 1000x1000x1000) cuando tracker.exit; unsuscribe on exit
    }

    public override void Refresh()
    {
        //TODO::
        //if tracker on view then 'move to tracker position'
        base.Refresh();

        if (!m_owner.m_followTrackerWhileVisible)
            return;

        m_owner.transform.position = m_owner.m_relatedTrackersGOs[0].transform.position;
        m_owner.transform.rotation = m_owner.m_relatedTrackersGOs[0].transform.rotation;
    }

    public override void Exit()
    {
        base.Exit();
        m_owner.EvtCaller_OnExitVisible();
    }
    //TODO::
    //bool trackerOnView;
}
