﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGalleryController : MonoBehaviour
{


    public void LSTR_NextImage() {
        m_currentlyOnView++;
        if (m_currentlyOnView >= m_gdc.m_data.m_imageArray.Length)
            m_currentlyOnView = 0;
        m_gdc.m_image.sprite = m_gdc.m_data.m_imageArray[m_currentlyOnView];
    }

    public void LSTR_PrevImage() {
        m_currentlyOnView--;
        if (m_currentlyOnView < 0)
            m_currentlyOnView = m_gdc.m_data.m_imageArray.Length - 1;
        m_gdc.m_image.sprite = m_gdc.m_data.m_imageArray[m_currentlyOnView];
    }

    public void LSTR_MapBtn()
    {
        m_mapDisplayed = !m_mapDisplayed;

        if (m_mapDisplayed)
        {
            
            m_btnPrev.interactable = false;
            m_btnNext.interactable = false;

            m_gdc.m_image.sprite = m_gdc.m_data.m_image;
        } else
        {
            m_btnPrev.interactable = true;
            m_btnNext.interactable = true;

            m_gdc.m_image.sprite = m_gdc.m_data.m_imageArray[m_currentlyOnView];
        }


    }

    //////private void Awake()
    //////{
    //////    m_thisData = GetComponent<GalleryDataContainer>().m_data;
    //////}

    public Image m_targetImg;
    int m_currentlyOnView = 0;

    //////DisplayableDataWithGallery m_thisData;

    public Button m_btnNext;
    public Button m_btnPrev;
    public bool m_mapDisplayed = false;
    public GalleryDataContainer m_gdc;
}
