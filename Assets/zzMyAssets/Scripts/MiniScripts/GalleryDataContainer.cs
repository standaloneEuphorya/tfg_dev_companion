﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryDataContainer : MonoBehaviour
{
    public GalleryDataContainer Init (DisplayableDataWithGallery thisObjectData)
    {
        m_image.sprite = thisObjectData.m_imageArray[0];
        m_name.text = thisObjectData.m_name;
        m_description.text = thisObjectData.m_description;

        m_data = thisObjectData;

        return this;
    }


    [SerializeField]
    public Image m_image;
    [SerializeField]
    Text m_name;
    [SerializeField]
    Text m_description;

    

    public RectTransform m_rectTransform;

   public  DisplayableDataWithGallery m_data;
}
